/**
 * Created by michaelmatias on 4/15/16.
 */

angular.module(controllersModuleName).controller('HomeCtrl', function($scope, $http, Restangular) {

  $scope.channelId = undefined;

  Restangular.all('categories').getList().then(function(data){
    $scope.categories=data;
  });

  $scope.getChannelId = function() {
    $scope.channelId = 1;
  };

  $scope.findChat = function(category) {

    Restangular.one('channels?category=' + category.categoryName).get().then(function (data) {
      console.log(category.categoryName);
      if (data != [] && data.isAvailable) {

            Restangular.all('assign-user-to-channel').post({
              channelId: data.channelId,
              userId: Math.floor((Math.random() * 100) + 1)
         });


        chID = data.channelId;
        console.log(chID);
        window.location = "#/tab/chat/" + chID + "/" + category.categoryName;

      } else {

        console.log("no open channel for this category, open new channel");

        Restangular.one('get-next-channel-id').get().then(function (data) {
          var add = {
            category: category.categoryName,
            channelId: data,
            isAvailable: true

          };

          Restangular.all('channels').post(add).then(function(){
            window.location = "#/tab/chat/" + data +"/" + category.categoryName;


          });

        });



      }

    });


  };










  $scope.getChannelId();

  navigator.geolocation.getCurrentPosition(function(position) {
    $http.get('http://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude + ',' + position.coords.longitude + '&sensor=false')
      .success(function(location) {
        $scope.city = location.results[3].address_components[0].short_name;
      });
  })


});
