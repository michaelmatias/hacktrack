/**
 * Created by michaelmatias on 4/15/16.
 */
angular.module(controllersModuleName).controller('ChatDetailCtrl', function($scope, $stateParams, $http, $timeout, $ionicScrollDelegate, Pubnub, $ionicModal) {
  $scope.channelId = $stateParams.channelId;
  $scope.categoryName = $stateParams.categoryName;
  $scope.messages = [];
  $scope.partnerId = "34";
  $scope.myId = "12";

  $scope.other = 'otherSender';

  $scope.data = {
    "sender" : $scope.myId,
    "message" : "",
    "time" : undefined
  };

  $scope.sendMessage = function(message) {
    $scope.data.time = new Date();

    Pubnub.publish({
      channel: '1',
      message : $scope.data,
      triggerEvents: false
    });
  };

  $scope.subscribe = function () {
    Pubnub.subscribe({
      channel: '1',
      triggerEvents: ['callback', 'connect']
    })
  };
  $scope.subscribe();

  $scope.$on(Pubnub.getMessageEventNameFor('1'), function (ngEvent, message, envelope, channelOrGroup, time, channel) {
    $scope.messages.push(
      {
        "sender" : message["sender"],
        "text" : message["message"],
        "time" : message["time"]
      }
    );
    $ionicScrollDelegate.scrollBottom(true);
  });

  $scope.$on(Pubnub.getEventNameFor('publish', 'callback'), function (ngEvent, message) {
    console.log(message);
  });

  $scope.getMessageClass = function(userId) {
    if (userId != $scope.myId) {
      return "otherSender";
    }
  };

  $ionicModal.fromTemplateUrl('../../templates/modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });
});
