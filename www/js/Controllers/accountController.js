/**
 * Created by michaelmatias on 4/15/16.
 */

angular.module(controllersModuleName).controller('AccountCtrl', function($scope, $ionicModal) {
  $scope.settings = {
    enableFriends: true
  };

  $ionicModal.fromTemplateUrl('../../templates/submittedFeedbackModal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });
});

