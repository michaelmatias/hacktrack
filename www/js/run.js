/**
 * Created by michaelmatias on 4/15/16.
 */
angular.module(mainApp).run(function($ionicPlatform, Pubnub) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });

  Pubnub.init({
    publish_key: 'pub-c-1e231e5f-955f-4a4e-bec2-8eb2ed396e33',
    subscribe_key: 'sub-c-6210c4b4-02ed-11e6-8cfb-0619f8945a4f'
  })
});
