var express = require('express');
var restful = require('node-restful');
var bodyparser = require('body-parser');
var methodOverride = require('method-override');
var morgan = require('morgan');
var cors = require('cors');

var mongoose = restful.mongoose;

var app = express();



app.use(cors());
app.use(morgan('dev'));
app.use(bodyparser.urlencoded({'extended': 'true'}));
app.use(bodyparser.json());
app.use(bodyparser.json({type: 'application/vnd.api+json'}));
app.use(methodOverride());
app.use(express.static('public'));
app.use(express.static('client'));
//needs to be client?


app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});


// <<-- DB -->>
mongoose.connect('mongodb://172.13.2.39/hacktrack');


// Define schema
var Schema = mongoose.Schema;
var usersSchema = new Schema({
  fullname: String,
  isReported: Boolean,
  RegisterAt: {type: Date, default: Date.now}
});

var channelsSchema = new Schema({
  channelId: String,
  category: String,
  activeUsers: [String],
  isAvailable: Boolean
});

var categorySchema = new Schema({
  categoryName: String,
  pictureUrl: String,
  description: String
});

// define AUTO basic REST API
var users = app.soldiers = restful.model('users', usersSchema). methods(['get', 'post', 'put', 'delete']);
users.register(app, '/users');

var channels = app.soldiers = restful.model('channels', channelsSchema). methods(['get', 'post', 'put', 'delete']);
channels.register(app, '/channels');

var categories = app.soldiers = restful.model('categories', categorySchema). methods(['get', 'post', 'put', 'delete']);
categories.register(app, '/categories');





/*
// define the models of the Schema
var volsModel = mongoose.model('vols',volsSchema);
var soldModel = mongoose.model('sold', soldiersSchema);

*/

var channelsModel = mongoose.model('channels', channelsSchema);
// var categoriesModel = mongoose.model('categories', channelsSchema);
// var usersModel = mongoose.model('users', channelsSchema);

app.get('/get-next-channel-id', function(req, res) {
  var nextChannelId = undefined;

  channelsModel.find().sort({channelId: -1}).limit(1).exec(function (err, vol) {
    var latestChannelId = vol[0]._doc.channelId;
    var nextChannelId = latestChannelId + 1;
    res.send(nextChannelId);
  });
});


app.post('/assign-user-to-channel', function(req, res) {
  var userToAssign = req.body.userId;
  var channelToAssign = req.body.channelId;
//  channelsModel.update({channelId : channelToAssign}, {$addToSet: {activeUsers : userToAssign},  isAvailable: false}).exec(function(err, vol) {

  channelsModel.update({channelId : channelToAssign}, {isAvailable: false},{},function(err, vol){

    res.send(vol);


  });
});

/*



// <<-- Custom REST -->>



app.get('/currectuser', function(req, res){

  var currectuser = {
    name: req.Cuser,
    groups: req.Cgroupes

};
  res.json(currectuser);

});


// POST: add user to the voletirng
app.post('/soldiers', function (req, res) {
  volsModel.findOne({
    'name': req.body.name
  }, function (err, vol) {
    if (err) return handleError(err);
    var solduser = vol.soldier.filter(function(sold){
      return sold.name === req.Cuser;
    }).pop();
    if(solduser != null && solduser.name == req.Cuser) return res.end("the user: " + req.Cuser + " is alraedy  "+ req.body.name);

    var userreg = vol.soldier.push({name: req.Cuser});
    vol.save(function (err) {
      if (err) return handleError(err);
    });
    res.end("add user");

  });
});


// DELETE :

app.delete('/soldiers', function(req, res){

  volsModel.findOne({'name': req.param('name')} , function(err, vol){
   // if (err) return handleError(err);
    if(err){
      console.log("no found");

    };


    var solduser = vol.soldier.filter(function(sold){
      return sold.name === req.Cuser;
    }).pop();
    if(!solduser) return res.end("no user: " + req.Cuser + " from: "+ req.body.name);
    solduser.remove();
    vol.save(function(err){
      if(err) return handleError(err);
      console.log('save');
    });


    res.end("remove user: " + req.Cuser + " from: "+ req.body.name);
  });
});





// GET Users for each volentir

app.get('/soldiers/:vol', function(req,res){

  volsModel.findOne({'name': req.params.vol} , function(err, vol){
    // if (err) return handleError(err);
    if(!vol) return res.end("no found: " + req.params.vol);
    res.json(vol.soldier);
  });


});




// Not in Use / GET return boolean if the user registerd to the volenting

app.get('/soldiers/', function(req,res){


  volsModel.findOne({'name': req.param('name') } , function(err, vol){
    // if (err) return handleError(err);
    if(!vol) return res.end("no found: " + req.param('name'));
    var i =0;
    for(; i< vol.soldier.length; i++){

      if(vol.soldier[i].name== req.param('user')) return res.json(true);;



    };
    res.json(false);
  });



});







// send back the single view

app.get('*', function(req, res){
  res.sendfile(root +"client/index.html");

});
*/



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

module.exports = app;
